const path = require('path')

const ROOT = path.resolve(__dirname, '../')

const env = process.env.NODE_ENV || 'development'
const isProd = env === 'production'
const isDev = !isProd

function root (args) {
  args = Array.prototype.slice.call(arguments, 0)
  return path.join.apply(path, [ROOT].concat(args))
}

exports.env = env
exports.isProd = isProd
exports.isDev = isDev
exports.root = root
