const webpack = require('webpack')
const merge = require('webpack-merge')
const base = require('./webpack.base.conf')
const helpers = require('./helpers')
const root = helpers.root

base.entry.examples = root('examples/app.js')

module.exports = merge.smart(base, {
  devServer: {
    historyApiFallback: true,
    noInfo: true
  },
  devtool: 'cheap-module-source-map',
  performance: {
    hints: false
  },
  plugins: [
    new webpack.LoaderOptionsPlugin({
      debug: true
    })
  ]
})

