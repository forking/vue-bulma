import Vue from 'vue'
import VueBulma from '../src'
import Router from 'vue-router'
import PageNotFoundView from './views/PageNotFound'
import HomeView from './views/Home'
import MixinsView from './views/overview/Mixins'
import VariablesView from './views/overview/Variables'
import SyntaxView from './views/modifiers/Syntax'
import HelpersView from './views/modifiers/Helpers'

import ColumnsView from './views/raw/grid/Columns'
import TilesView from './views/raw/grid/Tiles'
import ContainerView from './views/raw/layout/Container'
import FooterView from './views/raw/layout/Footer'
import HeroView from './views/raw/layout/Hero'
import SectionView from './views/raw/layout/Section'
import ElementsView from './views/raw/elements'

import ButtonView from './views/elements/Button'
import DeleteView from './views/elements/Delete'
import FormView from './views/elements/Form'
import IconView from './views/elements/Icon'
import ImageView from './views/elements/Image'
import NotificationView from './views/elements/Notification'
import ProgressView from './views/elements/Progress'
import TableView from './views/elements/Table'
import TagView from './views/elements/Tag'
import TitleView from './views/elements/Title'
import CardView from './views/components/Card'
import LevelView from './views/components/Level'
import MediaObjectView from './views/components/MediaObject'
import MenuView from './views/components/Menu'
import MessageView from './views/components/Message'
import ModalView from './views/components/Modal'
import NavView from './views/components/Nav'
import PaginationView from './views/components/Pagination'
import PanelView from './views/components/Panel'
import TabsView from './views/components/Tabs'

import App from './App'

Vue.use(VueBulma)
Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [{
    path: '/',
    name: 'home',
    component: HomeView
  }, {
    path: '/overview/mixins',
    name: 'mixins',
    component: MixinsView
  }, {
    path: '/overview/variables',
    name: 'variables',
    component: VariablesView
  }, {
    path: '/modifiers/syntax',
    name: 'syntax',
    component: SyntaxView
  }, {
    path: '/modifiers/helpers',
    name: 'helpers',
    component: HelpersView
  }, {
    path: '/grid/columns',
    name: 'columns',
    component: ColumnsView
  }, {
    path: '/grid/tiles',
    name: 'tiles',
    component: TilesView
  }, {
    path: '/layout/container',
    name: 'container',
    component: ContainerView
  }, {
    path: '/layout/footer',
    name: 'footer',
    component: FooterView
  }, {
    path: '/layout/hero',
    name: 'hero',
    component: HeroView
  }, {
    path: '/layout/section',
    name: 'section',
    component: SectionView
  }, {
    path: '/elements',
    name: 'elements',
    component: ElementsView
  }, {
    path: '/elements/button',
    name: 'button',
    component: ButtonView
  }, {
    path: '/elements/form',
    name: 'form',
    component: FormView
  }, {
    path: '/elements/icon',
    name: 'icon',
    component: IconView
  }, {
    path: '/elements/image',
    name: 'image',
    component: ImageView
  }, {
    path: '/elements/notification',
    name: 'notification',
    component: NotificationView
  }, {
    path: '/elements/progress',
    name: 'progress',
    component: ProgressView
  }, {
    path: '/elements/table',
    name: 'table',
    component: TableView
  }, {
    path: '/elements/tag',
    name: 'tag',
    component: TagView
  }, {
    path: '/elements/title',
    name: 'title',
    component: TitleView
  }, {
    path: '/components/card',
    name: 'card',
    component: CardView
  }, {
    path: '/components/level',
    name: 'level',
    component: LevelView
  }, {
    path: '/components/media-object',
    name: 'mediaObject',
    component: MediaObjectView
  }, {
    path: '/components/menu',
    name: 'menu',
    component: MenuView
  }, {
    path: '/components/message',
    name: 'message',
    component: MessageView
  }, {
    path: '/components/modal',
    name: 'modal',
    component: ModalView
  }, {
    path: '/components/nav',
    name: 'nav',
    component: NavView
  }, {
    path: '/components/pagination',
    name: 'pagination',
    component: PaginationView
  }, {
    path: '/components/panel',
    name: 'panel',
    component: PanelView
  }, {
    path: '/components/tabs',
    name: 'tabs',
    component: TabsView
  }, {
    path: '*',
    name: 'pageNotFound',
    component: PageNotFoundView
  }]
})

// create the app instance.
new Vue({
  router,
  render: h => h(App)
}).$mount('app')
