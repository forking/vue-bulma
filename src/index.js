import './styles/index.sass'
import './styles/index.styl'
import BulmaElements from './elements'
import BulmaComponents from './components'

export default {
  install (Vue, options) {
    Object.keys(BulmaElements).forEach(key => Vue.component(`Bulma${key}`, BulmaElements[key]))

    Object.keys(BulmaComponents).forEach(key => Vue.component(`Bulma${key}`, BulmaComponents[key]))

    // Vue.prototype.$notify = Notify
    // Vue.prototype.$modal = MessageModal
  }
}
