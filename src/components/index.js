import { Level, LevelItem } from './Level'
import Media from './Media.vue'

const BulmaComponents = {
  Level,
  LevelItem,
  Media
}

export default BulmaComponents
