import A from './A.vue'
import Button from './Button.vue'
import ButtonAddons from './ButtonAddons.vue'
import ButtonGroup from './ButtonGroup.vue'
import Control from './Control.vue'
import Icon from './Icon.vue'
import Tag from './Tag.vue'
import Title from './Title.vue'
import Subtitle from './Subtitle.vue'

const BulmaElement = {
  A,
  Button,
  ButtonAddons,
  ButtonGroup,
  Control,
  Icon,
  Tag,
  Title,
  Subtitle
}

export default BulmaElement
